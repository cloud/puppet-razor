# @param tmp_var              tmp param
# @param ip_address              tmp param
# @param netmask              tmp param
# @param gateway              tmp param
# @param mac_address              tmp param

class provisioning_host(

  Stdlib::IP::Address::V4::Nosubnet          $ip_address        = '127.0.0.1',
  Stdlib::IP::Address::V4                    $netmask           = '255.255.255.255',
  Stdlib::IP::Address::V4::Nosubnet          $gateway           = '127.0.0.1',
  Stdlib::MAC                                $mac_address       = '00:00:00:00:00:00',
  Stdlib::HTTPUrl                            $puppet_repo       = 'https://yum.puppetlabs.com/puppet5/puppet5-release-el-7.noarch.rpm',
  String[1]                                  $part,

) {
# install packages
package { 'ruby': ensure => 'installed' }
package { 'ruby-devel': ensure => 'installed' }

exec { 'yum Group Install':
  unless  => '/usr/bin/yum grouplist "Development tools" | /bin/grep "^Installed Groups"',
  command => '/usr/bin/yum -y groupinstall "Development tools"',
}

### configure DHCP
class { 'dhcp':
  service_ensure => running,
  nameservers  => ['192.168.10.1'],
  ntpservers   => ['us.pool.ntp.org'],
  interfaces   => ['eth0'],
  pxefilename  => 'pxelinux.0',
  pxeserver    => '192.168.10.102',
  ipxe_filename  => 'undionly.kpxe',
  ipxe_bootstrap => 'bootstrap.ipxe',
}

dhcp::host { 'test':
  comment => 'Optional descriptive comment',
  mac     => '52:54:00:89:82:1A',
  ip      => '192.168.10.103',
}

dhcp::pool{ 'test.local':
  network => '192.168.10.0',
  mask    => '255.255.255.0',
  gateway => '192.168.10.1',
}



### configure Postgres

    class { '::postgresql::server': }

#    postgresql::server::role { 'razoruser':
#    password_hash => postgresql_password('razoruser', '12345'),
#    }
   
#    postgresql::server::db { 'razor_prd':
#      owner    => 'razoruser',
#      user     => 'razoruser',
#      password => postgresql_password('razoruser', '12345'),
#    }

    postgresql::server::config_entry { 'listen_addresses':
      value => '*',
    }

    postgresql::server::pg_hba_rule { 'allow application network to access app database':
      description => 'Open up PostgreSQL for access from 192.168.10.0/24',
      type        => 'host',
      database    => 'all',
      user        => 'all',
      address     => '0.0.0.0/0',
#      address     => '192.168.10.0/24',
      auth_method => 'trust',
    }
#
#    postgresql::server::pg_hba_rule { 'allow application network to access app database':
#      description => 'Open up PostgreSQL for access from localhost',
#      type        => 'host',
#      database    => 'all',
#      user        => 'all',
#      address     => '127.0.0.1/32',
#      auth_method => 'trust',
#    }

      file { '/etc/yum.repos.d/puppet5.repo':
        content => template('razor/puppetlabs.repo.erb')
      }

### Razor

# Razor Configuration - Precompiled Microkernel
  class { 'razor':
    database_hostname   => '127.0.0.1',
    database_name       => 'razor_prd',
    database_username   => 'razoruser',
    database_password   => '12345',
    compile_microkernel => false,
    microkernel_url => 'http://links.puppetlabs.com/razor-microkernel-latest.tar',
 }

razor_broker { 'puppet':
  require => Class['razor'],
  ensure        => 'present',
  broker_type   => 'puppet',
  configuration => {
    'server'      => '192.168.10.101',
    'environment' => 'production'
  },
}

 class{ 'razor::api': }

razor_policy { 'install_centos_on_hypervisor':
require => Class['razor'],
  repo          => 'centos7',
  task          => 'centos/7',
  broker        => 'puppet',
  hostname      => 'test.test',
  root_password => '$6$lg.wOYebTcDaGR6K$gKDUDS1NEwiup2Xm91F35VQuMtKmaV//iec/qL3qC.oXHMHFXNvqfZkEVcVFYs46NSHT5pEBGLBzgUDUl9hq50',
  max_count     =>  20,
#  before_policy => 'policy0',
  node_metadata => {
           "ip_address"  => $ip_address,
           "netmask"     => $netmask,
           "gateway"     => $gateway,
           "mac_address" => $mac_address,
           "part"        => $part,
           },
  tags          => 'node01',
  enabled       => 'true',
  ensure        => 'present',
}


#create repo
razor_repo { 'centos7':
require => Class['razor'],
  ensure  => 'present',
  iso_url => 'http://ftp.fi.muni.cz/pub/linux/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-1810.iso',
  task    => 'centos/7',
}

#Create tag
razor_tag { 'node01':
require => Class['razor'],
  ensure => 'present',
  rule   => ['in', ['fact', 'macaddress'], $mac_address]
}

###Class Dependencies/Sequence
 Class['postgresql::server'] -> Class['razor']

}
class{ 'provisioning_host': }
